import express from 'express';
import * as courseController from '../controllers/courseControllers.js';

const router = express.Router();

// get all courses
router.get('/', courseController.getCourses);

// retrieve only active courses
router.get('/active-courses', courseController.getActiveCourses);

// create new course
router.post('/create-course', courseController.createCourse);

// get a specific course using findOne()
router.get('/specific-course', courseController.findCourse)

// get a specific course using getById()
router.get('/:id', courseController.getCourse)

// Update isActive status of the course using findByIdAndUpdate()
router.put("/:courseId/update-course-active", courseController.updateCourseActive);

export default router;