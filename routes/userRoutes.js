import express from 'express';
import { verify } from '../auth.js';
import * as userController from '../controllers/userControllers.js';

const router = express.Router();

router.get('/', userController.getUsers);
router.post('/register', userController.registerUser);
router.post('/login', userController.loginUser);
router.get('/details', verify, userController.getProfile);

export default router;