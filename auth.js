import jwt from 'jsonwebtoken';

// .sign(<data>, <secret_key>, {})
const SECRET_KEY = process.env.SECRET || "CourseBookingAPI";



export const createAccessToken = ({_id, email, isAdmin}) => {
    const data = { _id, email, isAdmin };

    return jwt.sign(data, SECRET_KEY);
}


export const verify = (req, res, next) => {
    // get the token in the headers authorization
    const token = req.headers.authorization.split(" ")[1];

    if(token) {
        return jwt.verify(token, SECRET_KEY, (err, data) => {
            if(err) {
                return res.send({ auth: "Failed" });
            } else {
                next();
            }
        });
    }
}

export const decode = (token) => {
    if(token) {
        return jwt.verify(token, SECRET_KEY, (err, data) => {
            if(err) {
                return res.send({ auth: "Failed" });
            } else {
                return jwt.decode(token, { complete: true }).payload;
            }
        });
    }
}
